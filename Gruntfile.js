module.exports = function (grunt) {
    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({

        "pkg": pkg,

        watch: {
            files: [
                'lib/**/*.js',
                'test/**/*.js'
            ],
            tasks: ['concat:dist']
        },

        "concat": {
            "options": {
                "banner": [
                    "/**",
                    " * WizardJS Widget - <%= pkg.version %>",
                    " * by <%= pkg.author %>",
                    " * @ <%= pkg.license %>",
                    " */\n\n"
                ].join('\n'),
                separator: '\n\n'
            },
            "dist": {
                "src": [
                    'umd/start',
                    'lib/static.js',
                    'lib/utils.js',
                    'lib/iterators.js',
                    'lib/prototype/**/*.js',
                    'lib/export.js',
                    'umd/end'
                ],
                "dest": 'dist/wizardjs-widget.js'
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('dev', ['concat:dist', 'watch']);
    grunt.registerTask('test', ['concat:dist']);
    grunt.registerTask('build', ['concat:dist']);
};