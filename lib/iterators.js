// ///////// //
// Iterators //
// ///////// //

/**
 * @static
 * @private
 * @function
 * @param    {Widget} widget
 */
function callDomReadyForWidget(widget) {
    widget._ready();
}

/**
 * @static
 * @private
 * @function
 * @param    {Widget} widget
 */
function callRemoveForWidget(widget) {
    widget.remove();
}

/**
 * @static
 * @private
 * @function
 * @param    {Widget} widget
 */
function callSuspendForWidget(widget) {
    widget.suspend();
}

/**
 * @static
 * @private
 * @function
 * @param    {Function} listener
 * @param    {String}   type
 */
function addListener(listener, type) {
    domHub.addListener(type);
    // ensuring event listener exists on
    // the prototype and not the instance
    if (typeof this._listeners === 'undefined') {
        this.private({
            _listeners: {}
        });
    }

    this._listeners[type] = listener;
}

/**
 *
 * @param {Element} domRoot
 * @param {Widget}  widget
 * @param {string}  name
 */
function renderChild(domRoot, widget, name) {
    var selector = this._renderMap[name] || this._renderMap['*'],
        childRoot;

    if (selector) {
        childRoot = domRoot.querySelector(selector);
    }

    widget._renderWidget(childRoot || domRoot);

    childRoot = domRoot = null;
}