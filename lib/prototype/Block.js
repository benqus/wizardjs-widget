/**
 * @class Block
 * @extends {wizard}
 */
var block = wizard.extend()
    .init(function (domRoot) {
        this.private({
            /**
             * @private
             * @type {Node}
             */
            _domRoot: domRoot,

            /**
             * @private
             * @type {Widget}
             */
            _widget: undefined
        });
    })
    .public({
        Widget: widget,

        getDOMNode: function () {
            return this._domRoot;
        },

        getWidget: function () {
            return this._widget;
        },

        render: function () {
            var domRoot = this.getDOMNode(),
                Widget  = this.Widget;

            if (!this._widget && domRoot instanceof Node) {
                this._widget = Widget.create.apply(Widget, arguments);
            }

            if (domRoot) {
                this._widget.renderWidget(domRoot);
            }

            return this;
        }
    });