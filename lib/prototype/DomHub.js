/**
 * @class    DomHub
 * @extends {wizard}
 */
var domHub = wizard.extend()
    .private({
        /**
         * @private
         * @namespace
         */
        _domListeners: {},

        /**
         * @private
         * @param   {Event}   event
         * @returns {Boolean} false
         */
        _domListener: function (event) {
            var node = findDOMParentWithAttribute(
                event.target, WIDGET_DATA_NAME);

            if (node) {
                bubbleEventToWidget(node, event);
            }

            node = null;

            return false;
        }
    })

    .public({
        /**
         * @param {String} type
         */
        addListener: function (type) {
            var listener;
            // add a listener to DOM events if it hasn't been added yet
            if (!this._domListeners[type]) {
                listener = this._domListener.bind(this);
                document.addEventListener(type, listener);
                this._domListeners[type] = listener;
            }
        },

        /**
         * @function
         * @param {String} type
         */
        removeListener: function (type) {
            document.removeEventListener(type, this._domListeners[type]);
            delete this._domListeners[type];
        }
    });