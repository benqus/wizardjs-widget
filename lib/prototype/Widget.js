/**
 * @class    Widget
 * @extends {wizard}
 */
var widget = wizard.extend()
    /**
     * @constructor
     */
    .init(function () {
        this.private({
            /**
             * child widgets
             * @namespace
             */
            _children: {},

            /**
             * @type {String}
             */
            _widgetId: this._generateWidgetId(),

            /**
             * @type {Element|undefined}
             */
            _domRoot: undefined,

            /**
             * child widget render mapping
             * @namespace
             */
            _renderMap: {}
        });

        widgetLookup[this._widgetId] = this;
    })

    .constant({
        /**
         * @constant
         * @type {String}
         */
        TYPE: 'widget',

        /**
         * @constant
         * @type {String}
         */
        CLASS: 'Widget'
    })

    .private({
        /**
         * Generates a new widget id
         * @private
         * @returns {String}
         */
        _generateWidgetId: function () {
            return widget.TYPE + (widgetId += 1);
        },

        /**
         * Returns all cssClass properties from the prototype-chain
         * if they are set on the actual prototype-level.
         * @private
         * @returns {Array}
         */
        _getCSSClasses: function () {
            var classes = [],
                object = this;

            do {
                object = Object.getPrototypeOf(object);
                // if cssClass is overwritten add it to the classes
                if (object.hasOwnProperty('CLASS')) {
                    classes.push(object.CLASS);
                }
            } while (object !== widget);

            return classes;
        },

        /**
         * @private
         * @param   {Element} elem
         */
        _prepareDomRoot: function (elem) {
            elem.setAttribute(WIDGET_DATA_NAME, this._widgetId);
            elem.setAttribute('class', this._getCSSClasses().join(' '));
            elem = null;
        },

        /**
         * @private
         * @param   {Element} parent
         */
        _renderWidget: function (parent) {
            // create new widget domRoot
            var domRoot = document.createElement(this.tagName);
            this._prepareDomRoot(domRoot);
            // render markup in the new element
            domRoot.innerHTML = (this.render() || '');
            // render child widgets
            this._renderChildren(domRoot);
            // destroy old widget domRoot
            if (this._domRoot) {
                disposeElement(this._domRoot
                    .parentNode.removeChild(this._domRoot));
                // force garbage collector to collect and
                // destroy the previous DOM Elements
                this._domRoot = null;
            }
            // append it into the DOM
            parent.appendChild(domRoot);
            // preserve new dom root for widget
            this._domRoot = domRoot;
            // reset scope variable references
            parent = domRoot = null;
        },

        /**
         * @private
         * @param   {Element} domRoot
         */
        _renderChildren: function (domRoot) {
            each(this._children, renderChild.bind(this, domRoot));
            domRoot = null;
        },

        /**
         * @private
         */
        _ready: function () {
            this.ready();
            each(this._children, callDomReadyForWidget);
        },

        _getListener: function (type) {
            return this._listeners[type];
        }
    })

    .public({
        /**
         * @type {String}
         */
        tagName: 'section',

        /**
         * Adds the widget into another one as a child
         * @param   {Widget}  parent    - parent widget to add the widget into
         * @param   {String}  name      - name of the widget
         * @param   {String} [selector] - (optional) render mapping selector
         * @returns {Widget}
         */
        addTo: function (parent, name, selector) {
            parent.addChild(this, name, selector);
            return this;
        },

        /**
         * Adds a new child widget
         * @param   {Widget}  child     - the child widget
         * @param   {String}  name      - name of the child
         * @param   {String} [selector] - (optional) render mapping selector
         * @returns {Widget}
         */
        addChild: function (child, name, selector) {
            if (child.is(widget)) {
                this._children[name] = child;
                this._renderMap[name] = selector;
                // if widget is already rendered into
                // the DOM render the child widget also
                if (this._domRoot) {
                    child.renderWidget(this._domRoot);
                }
            }

            return this;
        },

        /**
         * Gets a child of the Widget
         * @param   {String} name - name of the child widget
         * @returns {Widget}
         */
        getChild: function (name) {
            return this._children(name);
        },

        /**
         * Removes a new child widget
         * @param   {String} name
         * @returns {Widget}
         */
        removeChild: function (name) {
            var child = this._children[name];

            if (child) {
                child.remove();
            }

            return this;
        },

        /**
         * Removes the widget
         * @returns {Widget}
         */
        remove: function () {
            var domRoot = this.getDOMNode();

            if (domRoot) {
                // suspend all children after removing
                each(this._children, callSuspendForWidget);
                this.suspend();
                domRoot.parentNode.removeChild(domRoot);
                disposeElement(domRoot);
                domRoot = null;
            }
            // remove all children AFTER removing
            // the root causing one re-flow only
            each(this._children, callRemoveForWidget);

            delete widgetLookup[this._widgetId];

            return this;
        },

        /**
         * Renders the widget into the provided DOM root node
         * @param   {Element} parentDOM
         * @returns {Widget}
         */
        renderWidget: function (parentDOM) {
            // if widget is rendered, suspend it first
            if (this.getDOMNode()) {
                this.suspend();
            }

            this._renderWidget(parentDOM);
            this._ready();

            return this;
        },

        /**
         * Rerenderd the widget.
         * @returns {Widget}
         */
        reRender: function () {
            var domRoot = this.getDOMNode();

            // if widget is rendered (this._domRoot exists), rerendered
            if (domRoot && domRoot.parentNode) {
                this.renderWidget(domRoot.parentNode);
            }

            return this;
        },

        /**
         * A map of event listeners
         * @param   {object} events
         * @returns {Widget}
         */
        listenFor: function (events) {
            each(events, addListener, this);
            return this;
        },

        /**
         * Returns the rendered Node in the DOM representing the Widget.
         * @returns {Node}
         */
        getDOMNode: function () {
            return this._domRoot;
        },

        // //// ///////// //
        // user overrides //
        // //// ///////// //
        render: noOp,

        ready: noOp,

        suspend: noOp
    });