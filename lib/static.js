/**
 * @static
 * @private
 * @type {number}
 */
var widgetId = 0;

/**
 * @namespace widgetLookup
 * @type {Object}
 */
var widgetLookup = {};

/**
 * No-operation
 * @static
 * @private
 * @function
 */
var noOp = function () {
};

/**
 * @static
 * @private
 * @type    {String}
 */
var WIDGET_DATA_NAME = 'data-wizard-widget-id';

// /// /////// ///////// //
// DOM garbage collector //
// /// /////// ///////// //
var garbageDOM;
var load = window.onload;
window.onload = function (e) {
    if (typeof garbageDOM === 'undefined') {
        garbageDOM = document.createElement('div');
        garbageDOM.setAttribute('class', 'wizard-widget-garbageDOM');
        garbageDOM.style.display = 'none';
        document.body.appendChild(garbageDOM);
    }

    if (typeof load === 'function') {
        load(e);
    }
};