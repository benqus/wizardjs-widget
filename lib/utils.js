// ///////// //
// Utilities //
// ///////// //

/**
 * Should force a DOM element to be removed
 * @static
 * @private
 * @function
 * @param    {Element} element
 */
function disposeElement(element) {
    garbageDOM.appendChild(element);
    element = null;
    garbageDOM.innerHTML = '';
}

/**
 * Iterator helper
 * @static
 * @private
 * @function
 * @param    {Object|Array} collection
 * @param    {Function}     callback
 * @param    {Object}       thisArg
 */
function each(collection, callback, thisArg) {
    var i;

    if (collection && typeof collection === 'object') {
        for (i in collection) {
            if (collection.hasOwnProperty(i)) {
                callback.call(thisArg || collection[i],
                    collection[i], i, collection);
            }
        }
    } else if (collection instanceof Array) {
        collection.forEach(callback, thisArg || collection);
    }
}

/**
 * Runs up on the DOM looking for the first Element that has a 'data-wizard-widget-id' attribute.
 * @static
 * @private
 * @function
 * @param    {Element} element
 * @param    {String}  attribute
 * @returns  {Element}
 */
function findDOMParentWithAttribute(element, attribute) {
    var elem = element,
        value;

    while (elem && elem !== document) {
        value = elem.getAttribute(attribute);

        if (value) {
            return elem;
        }

        elem = elem.parentNode;
    }
}

/**
 * Determines whether an event can be bubbled up on the specified node.
 * @static
 * @private
 * @function
 * @param    {Element} element
 * @param    {Event}   event
 * @param    {Boolean} previousListenerResult
 * @returns  {Boolean}
 */
function eventCanBubbleOnNode(element, event, previousListenerResult) {
    return (
        element &&
        element !== document &&
        previousListenerResult !== false &&
        event.bubbles
        );
}

/**
 * Bubbles event to widgets based on the actual DOM.
 * @static
 * @private
 * @function
 * @param    {Element} element
 * @param    {Event}   event
 */
function bubbleEventToWidget(element, event) {
    var widgetId = element.getAttribute(WIDGET_DATA_NAME),
        widget = widgetLookup[widgetId],
        bubbles = true,
        listener;

    if (widget) {
        listener = widget._getListener(event.type);

        if (typeof listener === 'function') {
            bubbles = listener.call(widget, event);
        }

        element = findDOMParentWithAttribute(
            element.parentNode, WIDGET_DATA_NAME);

        if (eventCanBubbleOnNode(element, event, bubbles)) {
            bubbleEventToWidget(element, event);
        }
    }

    element = null;
}